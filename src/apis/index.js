import request from "superagent";
import { resolve, reject } from "q";

const authKey = process.env.REACT_APP_AUTH_KEY || "";

export default class Api {
  static getRestaurants(query, start) {
    return new Promise((resolve, reject) => {
      request
        .get(`https://developers.zomato.com/api/v2.1/search?${query}&start=${start}`)
        .set("User-Key", authKey)
        .set("Accept", "application/json")
        .end((error, res) => {
          error ? reject(error) : resolve(res);
        });
    });
  }

  static getLocationDetails(lat, lon) {
    return new Promise((resolve, reject) => {
      request
        .get(`https://developers.zomato.com/api/v2.1/geocode?lat=${lat}&lon=${lon}`)
        .set("User-Key", authKey)
        .set("Accept", "application/json")
        .end((error, res) => {
          error ? reject(error) : resolve(res);
        });
    });
  }

  static getRestaurantDetails(resId) {
    return new Promise((resolve, reject) => {
      request
        .get(`https://developers.zomato.com/api/v2.1/restaurant?res_id=${resId}`)
        .set("User-Key", authKey)
        .set("Accept", "application/json")
        .end((error, res) => {
          error ? reject(error) : resolve(res);
        });
    });
  }

  static getRestaurantReviews(resId) {
    return new Promise((resolve, reject) => {
      request
        .get(`https://developers.zomato.com/api/v2.1/reviews?res_id=${resId}`)
        .set("User-Key", authKey)
        .set("Accept", "application/json")
        .end((error, res) => {
          error ? reject(error) : resolve(res);
        });
    });
  }
}
