import React from "react";

class Navbar extends React.Component {
  render() {
    return (
      <div className="nav-bar">
        <div className="wrapper">Restaurant Finder</div>
      </div>
    );
  }
}

export default Navbar;
