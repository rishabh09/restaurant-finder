import React from "react";
import Navbar from "../Navbar";
import CitiesList from "../CitiesList";

class LandingPage extends React.Component {
  render() {
    return (
      <div>
        <Navbar />
        <div className="container">
          <div className="wrapper">
            <CitiesList />
          </div>
        </div>
      </div>
    );
  }
}

export default LandingPage;
