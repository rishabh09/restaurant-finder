import React from "react";
import qs from "qs";
import { connect } from "react-redux";
import Navbar from "../Navbar";
import ReviewDiv from "../ReviewDiv";

import { ReactComponent as Star } from "../../icons/star.svg";
import { getRestaurantDetails } from "../../actions/fetch-data";

class DetailsPage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { location, dispatch } = this.props;
    const { search } = location;
    const queryObj = qs.parse(search, { ignoreQueryPrefix: true });
    dispatch(getRestaurantDetails(queryObj.id));
  }
  render() {
    const { details, reviews } = this.props;
    const { list, total } = reviews;
    return (
      <div>
        <Navbar />
        <div className="wrapper restaurant-page-wrapper">
          <div className="restaurant-banner">
            <img src={details && details.featured_image} />
          </div>
          <div className="restaurant-page-container">
            <div className="restaurant-info-div">
              <div className="restaurant-name">{details && details.name}</div>
              <div className="restaurant-info">
                <div className="restaurant-rating">
                  <Star />
                  {details && details.user_rating ? (
                    <div className="text">
                      {details.user_rating.aggregate_rating} <span>({details.user_rating.votes} Votes)</span>
                    </div>
                  ) : null}
                  |
                </div>
                <div className="restaurant-review">{total} Reviews</div>
              </div>
            </div>
            <div className="restaurant-details-div">
              <div>
                <div className="details-title">Cost for 2</div>
                {details && details.average_cost_for_two ? (
                  <div className="details-text">
                    {details.currency} {details.average_cost_for_two}
                  </div>
                ) : null}
              </div>
              <div>
                <div className="details-title">Address</div>
                <div className="details-text">{details && details.location && details.location.address}</div>
              </div>
            </div>
            {/* <div className="restaurant-photos-div" />
            <div className="restaurant-menu-div" /> */}
            <div className="restaurant-ratings-div">
              <div className="heading">Ratings</div>
              {list &&
                list.map(({ review }) => {
                  return <ReviewDiv key={review.id} review={review} />;
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ details, reviews }) => ({ details, reviews });

export default connect(mapStateToProps)(DetailsPage);
