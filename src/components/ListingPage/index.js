import React from "react";
import qs from "qs";
import { connect } from "react-redux";

import Navbar from "../Navbar";
import RestaurantDiv from "../RestaurantDiv";

import { getRestaurantsList } from "../../actions/fetch-data";

class ListingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { page: 0, hasMore: true, isLoading: false };
    this.onScroll = this.onScroll.bind(this);
    this.loadRestaurants = this.loadRestaurants.bind(this);
  }

  onScroll() {
    const { isLoading, hasMore } = this.state;

    if (isLoading || !hasMore) return;

    if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
      // Do awesome stuff like loading more content!
      this.loadRestaurants();
    }
  }

  loadRestaurants() {
    this.setState({ isLoading: true }, () => {
      const { page } = this.state;
      const { location, dispatch, restaurants } = this.props;
      const { search } = location;
      const queryObj = qs.parse(search, { ignoreQueryPrefix: true });
      dispatch(
        getRestaurantsList({ page, ...queryObj }, restaurants.list, results => {
          if (results) {
            this.setState({ hasMore: true, isLoading: false, page: page + 1 });
          } else {
            this.setState({ hasMore: false, isLoading: false });
          }
        })
      );
    });
  }
  componentDidMount() {
    this.loadRestaurants();
    document.addEventListener("scroll", this.onScroll);
  }

  componentWillUnmount() {
    document.removeEventListener("scroll", this.onScroll);
  }
  render() {
    const { address, restaurants } = this.props;
    const { list } = restaurants;
    return (
      <div>
        <Navbar />
        <div className="wrapper">
          <div className="restaurants-list-div">
            <div className="heading">
              Showing Restaurants in <strong>"{address}"</strong>
            </div>
            <div className="restaurants-div">
              {(list &&
                list.map(({ restaurant }) => {
                  const key = restaurant.id;
                  return <RestaurantDiv key={key} data={restaurant} />;
                })) ||
                null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ address, restaurants }) => ({ address, restaurants });

export default connect(mapStateToProps)(ListingPage);
