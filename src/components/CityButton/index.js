import React from "react";

const CityButton = ({ city }) => {
  const { name } = city;
  return (
    <a href={`/list?city=${name.toLowerCase()}`} className="city-btn">
      {name}
    </a>
  );
};

export default CityButton;
