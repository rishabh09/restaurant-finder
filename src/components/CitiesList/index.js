import React from "react";
import { withRouter } from "react-router-dom";

import CityButtom from "../CityButton";

const cities = [{ name: "Bangalore" }, { name: "Mumbai" }, { name: "Delhi" }, { name: "Pune" }, { name: "Hyderabad" }];

class CitiesList extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const { history } = this.props;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        const { latitude, longitude } = position.coords;
        const nextUrl = `/list?lat=${latitude}&lon=${longitude}`;
        history.push(nextUrl);
      });
    } else {
      alert("Can't Find Location");
    }
  }

  render() {
    return (
      <div className="cities-list-div">
        <div className="heading">Select City</div>
        <div className="cities-list">
          {cities.map((city, i) => {
            return <CityButtom key={i} city={city} />;
          })}
        </div>
        <button className="city-btn" onClick={this.onClick}>
          Detect Location
        </button>
      </div>
    );
  }
}

export default withRouter(CitiesList);
