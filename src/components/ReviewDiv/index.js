import React from "react";

import { ReactComponent as Star } from "../../icons/star.svg";
import { ReactComponent as EmptyStar } from "../../icons/empty-star.svg";

const ReviewDiv = ({ review }) => {
  const { review_text, user, rating } = review;
  const stars = [...Array(rating)];
  const emptyStar = [...Array(5 - rating)];
  return (
    <div className="review-div">
      <div className="review-profile">
        <img src={user.profile_image} />
      </div>
      <div className="review-details">
        <div className="review-name">{user.name}</div>
        <div className="review-stars">
          {stars.map((x, i) => {
            return <Star key={i} />;
          })}
          {emptyStar.map((x, i) => {
            return <EmptyStar key={i} />;
          })}
        </div>
        <div className="review-text">{review_text}</div>
      </div>
    </div>
  );
};

export default ReviewDiv;
