import React from "react";
import { ReactComponent as Star } from "../../icons/star.svg";

const RestaurantDiv = ({ data }) => {
  const { name, user_rating, featured_image, thumb, id } = data;
  return (
    <a href={`/details?id=${id}`} className="restaurant-div">
      <div className="restaurant-image">
        <img src={thumb || featured_image || "https://picsum.photos/200/200"} />
      </div>
      <div className="restaurant-details">
        <div className="restaurant-name">{name}</div>
        <div className="restaurant-info">
          <div className="restaurant-rating">
            <Star />
            <div className="text">
              {user_rating.aggregate_rating} <span>({user_rating.votes} Votes)</span>
            </div>
            |
          </div>
          <div className="restaurant-review">187 Reviews</div>
        </div>
      </div>
    </a>
  );
};

export default RestaurantDiv;
