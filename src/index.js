import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

import LandingPage from "./components/LandingPage";
import ListingPage from "./components/ListingPage";
import DetailsPage from "./components/DetailsPage";

import * as serviceWorker from "./serviceWorker";
import configureStore from "./store";

import "./index.css";

const store = configureStore();

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/" exact component={LandingPage} />
            <Route path="/list" exact component={ListingPage} />
            <Route path="/details" exact component={DetailsPage} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
