import { SET_LIST, SET_NEXT } from "../actions/constants";

const initState = {
  list: [],
  next: true
};

export default function setList(state = initState, action) {
  switch (action.type) {
    case SET_LIST:
      return { ...state, list: action.payload };
    case SET_LIST:
      return { ...state, next: action.payload };
    default:
      return state;
  }
}
