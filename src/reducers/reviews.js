import { SET_REVIEWS } from "../actions/constants";

const initState = {
  list: null,
  total: 0
};

export default function setReviews(state = initState, action) {
  switch (action.type) {
    case SET_REVIEWS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
