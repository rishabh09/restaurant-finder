import { SET_DETAILS } from "../actions/constants";

export default function setDetails(state = null, action) {
  switch (action.type) {
    case SET_DETAILS:
      return action.payload;
    default:
      return state;
  }
}
