import { SET_ADDRESS } from "../actions/constants";

export default function setAddress(state = "", action) {
  switch (action.type) {
    case SET_ADDRESS:
      return action.payload;
    default:
      return state;
  }
}
