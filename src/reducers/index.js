import { combineReducers } from "redux";

import restaurants from "./restaurants";
import address from "./address";
import details from "./details";
import reviews from "./reviews";

const rootReducer = combineReducers({
  restaurants,
  address,
  details,
  reviews
});

export default rootReducer;
