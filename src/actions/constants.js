export const SET_LIST = "SET_LIST";
export const SET_NEXT = "SET_NEXT";
export const SET_ADDRESS = "SET_ADDRESS";
export const SET_DETAILS = "SET_DETAILS";
export const SET_REVIEWS = "SET_REVIEWS";
