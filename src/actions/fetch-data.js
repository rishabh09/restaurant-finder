import Api from "../apis";
import * as Type from "./constants";

export const getRestaurantsList = (data, list = [], cb) => dispatch => {
  let query = "";
  const { city, lat, lon, page } = data;
  const start = page * 20;
  if (city) {
    query = query.concat(`q=${city}`);
    dispatch({ type: Type.SET_ADDRESS, payload: city });
  } else {
    query = query.concat(`lat=${lat}&lon=${lon}`);
    dispatch(getLocation(lat, lon));
  }
  Api.getRestaurants(query, start)
    .then(res => {
      const { body } = res;
      const { restaurants, results_shown } = body;
      const newList = list.concat(restaurants);
      dispatch({ type: Type.SET_LIST, payload: newList });
      cb(results_shown);
    })
    .catch(err => {
      cb(0);
      console.log("err", err);
    });
};

export const getLocation = (lat, lon) => dispatch => {
  Api.getLocationDetails(lat, lon)
    .then(res => {
      const { body } = res;
      const { location } = body;
      const { title, city_name } = location;
      dispatch({ type: Type.SET_ADDRESS, payload: `${title}, ${city_name}` });
    })
    .catch(err => {
      console.log("err", err);
    });
};

export const getRestaurantDetails = resId => dispatch => {
  dispatch(getRestaurantReviews(resId));
  Api.getRestaurantDetails(resId)
    .then(res => {
      const { body } = res;
      dispatch({ type: Type.SET_DETAILS, payload: body });
    })
    .catch(err => {
      console.log("err", err);
    });
};

export const getRestaurantReviews = resId => dispatch => {
  Api.getRestaurantReviews(resId)
    .then(res => {
      const { body } = res;
      const { user_reviews, reviews_count } = body;

      const payload = {
        list: user_reviews,
        total: reviews_count
      };
      dispatch({ type: Type.SET_REVIEWS, payload });
    })
    .catch(err => {
      console.log("err", err);
    });
};
